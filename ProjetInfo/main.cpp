#include <SFML/Graphics.hpp>
#include <stdio.h>
#include<stdlib.h>
#include<iostream>
#include<string>
#include<math.h>
#define PI 3.1415926
using namespace std;
bool isSpriteHover(sf::FloatRect sprite, sf::Vector2f mp)
{
    if (sprite.contains(mp)) {
        return true;
    }
    return false;
}

int main()
{
    sf::Font font;
    if (!font.loadFromFile("digital-7.ttf"))
    {
        std::cerr << "Erreur ouverture police digital-7.ttf" << std::endl;
    }
    sf::Font font2;
    if (!font2.loadFromFile("arial.ttf"))
    {
        std::cerr << "Erreur ouverture police arial.ttf" << std::endl;
    }
    //Anti-alliasing permettant de "lisser" les formes et d'�viter aux lignes de disparaitre
    sf::ContextSettings settings;
    settings.antialiasingLevel = 4;

    sf::VideoMode DesktopMode = sf::VideoMode::getDesktopMode(); // Adaptation auto taille fenetre
    sf::RenderWindow window(DesktopMode, "Matematica", sf::Style::Fullscreen, settings); //Fenetre SFML
    // r�cup�ration de la taille de la fen�tre
    sf::Vector2u size = window.getSize();
    int width = size.x;
    int height = size.y;
    int tailledem = 40;
    int taillecase = (int)((width * 0.75) / tailledem);
    sf::View view(sf::Vector2f(width*0.625, height*0.625), sf::Vector2f(0.75 * width, 0.75 * height));
    view.setViewport(sf::FloatRect(0.25f, 0.25f, 0.75, 0.75f));
    view.setCenter(0, 0);
    view.zoom(2);
    int tmpx = width;
    int tmpy = height;
    string s;
    //FLECHES DEFINITION
    sf::Texture fleche;
    if (!fleche.loadFromFile("fleche.png"))
    {
        cout << "Erreur lors de la lecture du fichier fleche.png";
    }
    sf::Sprite flechesprited;
    flechesprited.setTexture(fleche);
    sf::Sprite flechespriteg;
    flechespriteg.setTexture(fleche);
    flechespriteg.rotate(180);
    sf::Sprite flechespriteh;
    flechespriteh.setTexture(fleche);
    flechespriteh.rotate(90);
    sf::Sprite flechespriteb;
    flechespriteb.setTexture(fleche);
    flechespriteb.rotate(-90);
    //FIN FLECHES
    //TEST FONCTION
    vector <float> fonctionval;
    float nbpoints = 2000; //Nombre de points pour le tracage
    int borneinf = -tailledem / 2; //Borne inf�rieur du graphe
    int bornesup = tailledem / 2; //Borne sup�rieure du graphe
    for (float x = borneinf; x < bornesup; x = x + (tailledem / nbpoints)) { //On stocke dans levector les resultats
        fonctionval.push_back(x * x); // LA FONCTION Y=X� dans le pushback
    }
    //CALCULATRICE
    sf::RectangleShape bouton0(sf::Vector2f(100, 50));// BOUTON 0
    bouton0.setSize(sf::Vector2f(width*0.25*0.25*0.9, 100));
    bouton0.setFillColor(sf::Color::Black);
    bouton0.setPosition(width * 0.02 * 0.25, height - width * 0.02 * 0.25 - 100);//POS A SET APRES ET CREER CLASSE
    sf::Text textbouton0("0", font2, 50);// TEXT BOUTON 0
    textbouton0.setPosition(width * 0.25 * 0.25 * 0.9*0.5- width * 0.02 * 0.125, height - width * 0.02 * 0.25 - 80);
    textbouton0.setFillColor(sf::Color::White);
    sf::Text textbouton1("1", font2, 50);// TEXT BOUTON 1
    textbouton1.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 - width * 0.02 * 0.125, height - width * 0.01 - 80-100);
    textbouton1.setFillColor(sf::Color::White);
    sf::Text textbouton2("2", font2, 50);// TEXT BOUTON 2
    textbouton2.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 1*width * 0.02 * 0.125+ width * 0.25 * 0.25 * 0.9, height - width * 0.01 - 80 - 100);
    textbouton2.setFillColor(sf::Color::White);
    sf::Text textbouton3("3", font2, 50);// TEXT BOUTON 3
    textbouton3.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 1.5 * width * 0.02 * 0.25 + width * 0.5 * 0.25 * 0.9, height - width * 0.01 - 80 - 100);
    textbouton3.setFillColor(sf::Color::White);
    sf::Text textbouton4("4", font2, 50);// TEXT BOUTON 4
    textbouton4.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 - width * 0.02 * 0.125, height -  width * 0.02 * 0.75 - 80 - 200);
    textbouton4.setFillColor(sf::Color::White);
    sf::Text textbouton5("5", font2, 50);// TEXT BOUTON 5
    textbouton5.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 1 * width * 0.02 * 0.125 + width * 0.25 * 0.25 * 0.9, height -width * 0.02 * 0.75 - 80 - 200);
    textbouton5.setFillColor(sf::Color::White);
    sf::Text textbouton6("6", font2, 50);// TEXT BOUTON 6
    textbouton6.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 1.5 * width * 0.02 * 0.25 + width * 0.5 * 0.25 * 0.9, height - width * 0.02 * 0.75 - 80 - 200 );
    textbouton6.setFillColor(sf::Color::White);
    sf::Text textbouton7("7", font2, 50);// TEXT BOUTON 7
    textbouton7.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 - width * 0.02 * 0.125, height - width * 0.02 - 80 - 300);
    textbouton7.setFillColor(sf::Color::White);
    sf::Text textbouton8("8", font2, 50);// TEXT BOUTON 8
    textbouton8.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 1 * width * 0.02 * 0.125 + width * 0.25 * 0.25 * 0.9, height -  width * 0.02  - 80 -300);
    textbouton8.setFillColor(sf::Color::White);
    sf::Text textbouton9("9", font2, 50);// TEXT BOUTON 9
    textbouton9.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 1.5 * width * 0.02 * 0.25 + width * 0.5 * 0.25 * 0.9, height - width * 0.02  - 80 - 300);
    textbouton9.setFillColor(sf::Color::White);
    sf::Text textboutonpoint(".", font2, 50);// TEXT BOUTON POINT
    textboutonpoint.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 1 * width * 0.02 * 0.125 + width * 0.25 * 0.25 * 0.9, height - width * 0.02 * 0.25 - 80);
    textboutonpoint.setFillColor(sf::Color::White);
    sf::Text textboutonvirgule(",", font2, 50);// TEXT BOUTON VIRGULE
    textboutonvirgule.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 1.5 * width * 0.02 * 0.25 + width * 0.5 * 0.25 * 0.9, height - width * 0.02 * 0.25 - 80);
    textboutonvirgule.setFillColor(sf::Color::White);
    sf::Text textboutonmoins("-", font2, 50);// TEXT BOUTON MOINS
    textboutonmoins.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 2.5 * width * 0.02 * 0.25 + width * 0.75 * 0.25 * 0.9, height - width * 0.02 * 0.25 - 80);
    textboutonmoins.setFillColor(sf::Color::White);
    sf::Text textboutonplus("+", font2, 50);// TEXT BOUTON PLUS
    textboutonplus.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 2.5 * width * 0.02 * 0.25 + width * 0.75 * 0.25 * 0.9, height - 2 * width * 0.02 * 0.25 - 80 - 100);
    textboutonplus.setFillColor(sf::Color::White);
    sf::Text textboutonfois("*", font2, 50);// TEXT BOUTON FOIS
    textboutonfois.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 2.5 * width * 0.02 * 0.25 + width * 0.75 * 0.25 * 0.9, height - 3 * width * 0.02 * 0.25 - 80 - 2 * 100);
    textboutonfois.setFillColor(sf::Color::White);
    sf::Text textboutondivise("/", font2, 50);// TEXT BOUTON DIVISE
    textboutondivise.setPosition(width * 0.25 * 0.25 * 0.9 * 0.5 + 2.5 * width * 0.02 * 0.25 + width * 0.75 * 0.25 * 0.9, height - 4 * width * 0.02 * 0.25 - 80 - 3 * 100);
    textboutondivise.setFillColor(sf::Color::White);
    sf::RectangleShape bouton1(sf::Vector2f(100, 50)); //BOUTON CALCULATRICE 1
    bouton1.setSize(sf::Vector2f(width * 0.25 * 0.25 * 0.9, 100));
    bouton1.setFillColor(sf::Color::Black);
    bouton1.setPosition(width * 0.02 * 0.25, height - width* 0.02 * 0.5 -200 ); // LE -100 = taille bouton
    sf::RectangleShape bouton2(sf::Vector2f(100, 50)); //BOUTON CALCULATRICE 2
    bouton2.setSize(sf::Vector2f(width * 0.25 * 0.25 * 0.9,100));
    bouton2.setFillColor(sf::Color::Black);
    bouton2.setPosition(width * 0.25 * 0.25 * 0.9 +width * 0.02 * 0.5, height - width * 0.02 * 0.5 - 200);
    sf::RectangleShape bouton3(sf::Vector2f(100, 50)); //BOUTON CALCULATRICE 3
    bouton3.setSize(sf::Vector2f(width * 0.25 * 0.25 * 0.9, 100));
    bouton3.setFillColor(sf::Color::Black);
    bouton3.setPosition(2* width * 0.25 * 0.25 * 0.9 + width * 0.02 * 0.75, height - width * 0.02 * 0.5 - 200);
    sf::RectangleShape bouton4(sf::Vector2f(100, 50));//BOUTON CALCULATRICE 4
    bouton4.setSize(sf::Vector2f(width * 0.25 * 0.25 * 0.9, 100));
    bouton4.setFillColor(sf::Color::Black);
    bouton4.setPosition(width * 0.02 * 0.25, height - width  * 0.02 * 0.75 - 300);
    sf::RectangleShape bouton5(sf::Vector2f(100, 50));//BOUTON CALCULATRICE 5
    bouton5.setSize(sf::Vector2f(width * 0.25 * 0.25 * 0.9, 100));
    bouton5.setFillColor(sf::Color::Black);
    bouton5.setPosition(width * 0.25 * 0.25 * 0.9 + width * 0.02 * 0.5, height - width  * 0.02 * 0.75 - 300);
    sf::RectangleShape bouton6(sf::Vector2f(100, 50)); //BOUTON CALCULATRICE 6
    bouton6.setSize(sf::Vector2f(width * 0.25 * 0.25 * 0.9, 100));
    bouton6.setFillColor(sf::Color::Black);
    bouton6.setPosition(2 * width * 0.25 * 0.25 * 0.9 + width * 0.02 * 0.75, height - width * 0.02 * 0.75 - 300);
    sf::RectangleShape bouton7(sf::Vector2f(100, 50)); //BOUTON CALCULATRICE 7
    bouton7.setSize(sf::Vector2f(width * 0.25 * 0.25 * 0.9, 100));
    bouton7.setFillColor(sf::Color::Black);
    bouton7.setPosition(width * 0.02 * 0.25, height - width * 0.02 - 400);
    sf::RectangleShape bouton8(sf::Vector2f(100, 50)); //BOUTON CALCULATRICE 8
    bouton8.setSize(sf::Vector2f(width * 0.25 * 0.25 * 0.9, 100));
    bouton8.setFillColor(sf::Color::Black);
    bouton8.setPosition(width * 0.25 * 0.25 * 0.9 + width * 0.02 * 0.5, height - width * 0.02  - 400);
    sf::RectangleShape bouton9(sf::Vector2f(100, 50)); //BOUTON CALCULATRICE 9
    bouton9.setSize(sf::Vector2f(width * 0.25 * 0.25 * 0.9, 100));
    bouton9.setFillColor(sf::Color::Black);
    bouton9.setPosition(2 * width * 0.25 * 0.25 * 0.9 + width * 0.02 * 0.75, height - width * 0.02 - 400);
    sf::RectangleShape boutonpoint(sf::Vector2f(100, 50)); //BOUTON CALCULATRICE .
    boutonpoint.setSize(sf::Vector2f(width * 0.25 * 0.25 * 0.9, 100));
    boutonpoint.setFillColor(sf::Color::Black);
    boutonpoint.setPosition(width * 0.25 * 0.25 * 0.9 + width * 0.02 * 0.5, height - width  * 0.02 * 0.25 - 100);
    sf::RectangleShape boutonvirgule(sf::Vector2f(100, 50));//BOUTON CALCULATRICE ,
    boutonvirgule.setSize(sf::Vector2f(width * 0.25 * 0.25 * 0.9, 100));
    boutonvirgule.setFillColor(sf::Color::Black);
    boutonvirgule.setPosition(2 * width * 0.25 * 0.25 * 0.9 + width * 0.02 * 0.75, height - width  * 0.02 * 0.25 -  100);
    sf::RectangleShape boutonmoins(sf::Vector2f(100, 50)); //BOUTON CALCULATRICE -
    boutonmoins.setSize(sf::Vector2f(width * 0.25 * 0.25 * 0.9, 100));
    boutonmoins.setFillColor(sf::Color::Black);
    boutonmoins.setPosition(3 * width * 0.25 * 0.25 * 0.9 + width * 0.02 , height - width * 0.02 * 0.25 - 100);
    sf::RectangleShape boutonplus(sf::Vector2f(100, 50)); //BOUTON CALCULATRICE +
    boutonplus.setSize(sf::Vector2f(width * 0.25 * 0.25 * 0.9, 100));
    boutonplus.setFillColor(sf::Color::Black);
    boutonplus.setPosition(3 * width * 0.25 * 0.25 * 0.9 + width * 0.02 , height - width * 0.02 * 0.5 - 200);
    sf::RectangleShape boutonfois(sf::Vector2f(100, 50)); //BOUTON CALCULATRICE *
    boutonfois.setSize(sf::Vector2f(width * 0.25 * 0.25 * 0.9, 100));
    boutonfois.setFillColor(sf::Color::Black);
    boutonfois.setPosition(3 * width * 0.25 * 0.25 * 0.9 + width * 0.02, height - width * 0.02 * 0.75 - 300);
    sf::RectangleShape boutondivise(sf::Vector2f(100, 50)); //BOUTON CALCULATRICE /
    boutondivise.setSize(sf::Vector2f(width * 0.25 * 0.25 * 0.9, 100));
    boutondivise.setFillColor(sf::Color::Black);
    boutondivise.setPosition(3 * width * 0.25 * 0.25 * 0.9 + width * 0.02 , height - width * 0.02 - 400);
    //FIN CALCULATRICE
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::MouseWheelMoved && event.mouseWheel.x> width *0.25 && event.mouseWheel.y> height*0.25)
            {
                std::cout << "wheel movement: " << event.mouseWheel.delta << std::endl;
                //Zoom
                if (event.mouseWheel.delta > 0) {
                    view.zoom(0.9f);
                }
                //D�zoom
                else if (event.mouseWheel.delta < 0) {
                    view.zoom(1.1f);
                    
                }
                std::cout << "mouse x: " << event.mouseWheel.x << std::endl;
                std::cout << "mouse y: " << event.mouseWheel.y << std::endl;
            }
            if (event.type == sf::Event::TextEntered) { //Saisie TEXTE
                if (event.type == sf::Event::TextEntered) {
                    if (event.KeyPressed == sf::Keyboard::BackSpace && s.size() != 0) {
                        s.pop_back();
                        std::cout << s << std::endl;
                    }
                    else if (event.text.unicode < 128) {
                        s.push_back((char)event.text.unicode);
                        std::cout << s << std::endl;
                    }
                }
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace) && s.size() != 0)
            {
                s.pop_back();
                std::cout << s << std::endl;
            }
            if (isSpriteHover(flechespriteg.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    view.move(-100.f, 0);
                }
            }
            if (isSpriteHover(flechesprited.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    view.move(100.f, 0);
                }
            }
            if (isSpriteHover(flechespriteh.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    view.move(0, 100);
                }
            }
            if (isSpriteHover(flechespriteb.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    view.move(0, -100);
                }
            }
            if (isSpriteHover(bouton0.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                bouton0.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('0');
                    bouton0.setFillColor(sf::Color::Black);
                }
            }
            if (isSpriteHover(bouton1.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                bouton1.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('1');
                    bouton1.setFillColor(sf::Color::Black);
                }
            }
            if (isSpriteHover(bouton2.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                bouton2.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('2');
                    bouton2.setFillColor(sf::Color::Black);
                }
            }
            if (isSpriteHover(bouton3.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                bouton3.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('3');
                    bouton3.setFillColor(sf::Color::Black);
                }
            }
            if (isSpriteHover(bouton4.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                bouton4.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('4');
                    bouton4.setFillColor(sf::Color::Black);
                }
            }
            if (isSpriteHover(bouton5.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                bouton5.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('5');
                    bouton5.setFillColor(sf::Color::Black);
                }
            }
            if (isSpriteHover(bouton6.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                bouton6.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('6');
                    bouton6.setFillColor(sf::Color::Black);
                }
            }
            if (isSpriteHover(bouton7.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                bouton7.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('7');
                    bouton7.setFillColor(sf::Color::Black);
                }
            }
            if (isSpriteHover(bouton8.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                bouton8.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('8');
                    bouton8.setFillColor(sf::Color::Black);
                }
            }
            if (isSpriteHover(bouton9.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                bouton9.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('9');
                    bouton9.setFillColor(sf::Color::Black);
                }
            }
            if (isSpriteHover(boutonpoint.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                boutonpoint.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('.');
                    boutonpoint.setFillColor(sf::Color::Black);
                }
            }
            if (isSpriteHover(boutonvirgule.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                boutonvirgule.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back(',');
                    boutonvirgule.setFillColor(sf::Color::Black);
                }
            }
            if (isSpriteHover(boutonmoins.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                boutonmoins.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('-');
                    boutonmoins.setFillColor(sf::Color::Black);
                }
            }
            if (isSpriteHover(boutonplus.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                boutonplus.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('+');
                    boutonplus.setFillColor(sf::Color::Black);
                }
            }
            if (isSpriteHover(boutonfois.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                boutonfois.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('*');
                    boutonfois.setFillColor(sf::Color::Black);
                }
            }
            if (isSpriteHover(boutondivise.getGlobalBounds(), sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
            {
                boutondivise.setFillColor(sf::Color(110, 110, 110));
                if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
                {
                    s.push_back('/');
                    boutondivise.setFillColor(sf::Color::Black);
                }
            }
        }

        window.clear(sf::Color(255, 255, 255));
        window.setView(view);
        int ChiffreGrille = 0;
        char bufferX[4];
        char bufferY[4];
        //GRILLE
        for (int dessinergrille = 0; dessinergrille < tailledem + 1; dessinergrille++) {
            //GRILLE BATONS
            int tmp = taillecase * dessinergrille;
            int tmp2 = -tailledem/2 + dessinergrille;
            sf::RectangleShape lineh(sf::Vector2f((float)(tailledem * taillecase)*2, 1));
            lineh.setPosition((float)(-tailledem* taillecase), (-tailledem * taillecase)  +(dessinergrille * taillecase+ tmp));
            lineh.setFillColor(sf::Color(180, 180, 180));
            window.draw(lineh);
            sf::RectangleShape linev(sf::Vector2f(1,(tailledem * taillecase)*2));
            linev.setPosition((float)(-tailledem * taillecase) + (dessinergrille * taillecase+ tmp), (-tailledem * taillecase));
            linev.setFillColor(sf::Color(180, 180, 180));
            window.draw(linev);
            _itoa_s(tmp2, bufferX, 10);
            _itoa_s(-tmp2, bufferY, 10);
            sf::Text textH(bufferX, font2, 12);
            textH.setPosition(sf::Vector2f(-tailledem * taillecase+taillecase*dessinergrille*2-10, 0));
            textH.setFillColor(sf::Color(30, 30, 30));
            window.draw(textH);
            sf::Text textV(bufferY, font2, 12);
            textV.setPosition(sf::Vector2f(0, -tailledem  * taillecase + taillecase * dessinergrille*2));
            textV.setFillColor(sf::Color(30, 30, 30));
            window.draw(textV);
            // FIN GRILLE BATONS
        }
        //FIN TEST FONCTION 
        //AFFICHAGE FONCTION
        float ratio = tailledem / nbpoints;
        for (int j = 0; j < fonctionval.size()-1; j++) {
            float distance = sqrt(pow((borneinf + (j + 1) * (tailledem / nbpoints)) - (borneinf + j * (tailledem / nbpoints)), 2) + pow((fonctionval[j + 1] - fonctionval[j]), 2));// DISTANCE ENTRE A ET B
            sf::RectangleShape trace(sf::Vector2f(distance*taillecase, 1));
            trace.setPosition((float)((-2*bornesup*taillecase+2*j*ratio*taillecase)),- fonctionval[j] *taillecase*2);
            trace.setFillColor(sf::Color::Red);
            window.draw(trace);
        }
        //FIN FCTION
        sf::RectangleShape axeX(sf::Vector2f(100000, 3)); //AXE X
        axeX.setPosition((float)(-50000), 0);
        axeX.setFillColor(sf::Color(0, 0, 0));
        window.draw(axeX);
        sf::RectangleShape axeY(sf::Vector2f(3, 100000)); //AXE Y
        axeY.setPosition(0, -50000);
        axeY.setFillColor(sf::Color(0, 0, 0));
        window.draw(axeY);
        //FIN GRILLE
        //AFFICHAGE AUTRE
        window.setView(window.getDefaultView());
        sf::RectangleShape rectangleconsole(sf::Vector2f(width / 4, height));
        rectangleconsole.setPosition(0, 0);
        rectangleconsole.setFillColor(sf::Color(0, 240, 0));
        window.draw(rectangleconsole);
        sf::RectangleShape rectanglecalculette(sf::Vector2f(width, height / 4));
        rectanglecalculette.setPosition(0, 0);
        rectanglecalculette.setFillColor(sf::Color(0, 0, 0));
        window.draw(rectanglecalculette);
        sf::CircleShape triangleAxeY(10, 3);
        triangleAxeY.setFillColor(sf::Color(0, 0, 0));
        triangleAxeY.setPosition(width*0.625-9, height*0.26);
        window.draw(triangleAxeY);
        sf::CircleShape triangleAxeX(10, 3);
        triangleAxeX.rotate(90);
        triangleAxeX.setFillColor(sf::Color(0, 0, 0));
        triangleAxeX.setPosition(width *0.995, height * 0.625 - 8.5);
        window.draw(triangleAxeX);
        //AFFICHAGE TEXTE
        sf::Text text(s, font, 50);
        text.setPosition(sf::Vector2f(0, 135));
        text.setFillColor(sf::Color(255, 255, 255));
        window.draw(text);
        //AFFICHAGE FLECHES
        flechesprited.setScale(0.5, 0.5);
        flechesprited.setPosition(width * 0.30, height * 0.3);
        window.draw(flechesprited);
        flechespriteg.setScale(0.5, 0.5);
        flechespriteg.setPosition(width * 0.28, height * 0.33);
        window.draw(flechespriteg);
        flechespriteh.setScale(0.5, 0.5);
        flechespriteh.setPosition(width * 0.3, height * 0.33);
        window.draw(flechespriteh);
        flechespriteb.setScale(0.5, 0.5);
        flechespriteb.setPosition(width * 0.285, height * 0.3);
        window.draw(flechespriteb);
        //FIN AFFICHAGE
        window.draw(bouton0); //TRACAGE BOUTONS
        window.draw(textbouton0);
        window.draw(bouton1);
        window.draw(textbouton1);
        window.draw(bouton2);
        window.draw(textbouton2);
        window.draw(bouton3);
        window.draw(textbouton3);
        window.draw(bouton4);
        window.draw(textbouton4);
        window.draw(bouton5);
        window.draw(textbouton5);
        window.draw(bouton6);
        window.draw(textbouton6);
        window.draw(bouton7);
        window.draw(textbouton7);
        window.draw(bouton8);
        window.draw(textbouton8);
        window.draw(bouton9);
        window.draw(textbouton9);
        window.draw(boutonpoint);
        window.draw(textboutonpoint);
        window.draw(boutonvirgule);
        window.draw(textboutonvirgule);
        window.draw(boutonmoins);
        window.draw(textboutonmoins);
        window.draw(boutonplus);
        window.draw(textboutonplus);
        window.draw(boutonfois);
        window.draw(textboutonfois);
        window.draw(boutondivise);
        window.draw(textboutondivise);
        window.display();
    }

    return 0;
}